﻿create database DBZStoreDB;

use DBZStoreDB;

select * from tb_produto;
select * from tb_pedido;
select * from tb_pedido_item;


create table tb_produto (
	id_produto int primary key auto_increment,
    nm_produto varchar(100),
    vl_preco decimal(15,2)
);


create table tb_pedido (
	id_pedido int primary key auto_increment,
    nm_cliente varchar(100),
    ds_cpf varchar(100),
    dt_venda datetime
);


create table tb_pedido_item (
	id_pedido_item int primary key auto_increment,
    id_pedido int,
    id_produto int,
    foreign key (id_pedido) references tb_pedido (id_pedido),
    foreign key (id_produto) references tb_produto (id_produto)
);
