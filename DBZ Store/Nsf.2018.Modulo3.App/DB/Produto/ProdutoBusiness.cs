﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();



        public int Salvar(ProdutoDTO produto)
        {
            if (produto.Produto == string.Empty)
            {
                throw new ArgumentException("produto é obrigatório.");
            }

            if (produto.preço <= 0)
            {
                throw new ArgumentException("preço obrigatorio.");
            }
            return db.Salvar(produto);
        }

        public List<produtoDTO> Consultar(string Nome)
        {
            if (Nome == string.Empty)
            {
                Nome = string.Empty;
            }

            return db.Consultar(Nome);
        }
    }
}
